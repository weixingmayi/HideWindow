
// HideWindowDlg.h : header file
//

#pragma once
#include "TransparentPicCtrl.h"
#include <map>
#include <vector>
using namespace std;
// CHideWindowDlg dialog
class CHideWindowDlg : public CDialogEx
{
// Construction
public:
	CHideWindowDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_HIDEWINDOW_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	struct WindowAttribute{
		LONG exStyle;
		COLORREF crKey;
		BYTE bAlpha;
		DWORD dwFlags;
	};

	map<HWND, WindowAttribute>	m_mapWindowAttribute;
	int							m_nTransparency;
	CTransparentPicCtrl			m_wndPreviewCtrl;

	NOTIFYICONDATAW				m_NotifyIconData;

	vector<vector<UINT>>		m_vHotKeys;
// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()


	BOOL	SetHotKey();
	BOOL	UnSetHotKey();

	BOOL	IsModKey(UINT _key);
	CString GetKeyString(UINT _key);
	CString	GetHotKeyString(vector<UINT>& _keys);
	UINT	GetHotKeyMod(vector<UINT>& _keys);
	UINT	GetHotKeyVkey(vector<UINT>& _keys);

	void	HideActiveWindow(HWND _hWnd);
	void	RestoreWindows();
	void	OnSlideTranparency();
	int		GetAlpha();
	void	LoadSettings();
public:
	afx_msg void OnDestroy();
	afx_msg void OnHotKey(UINT nHotKeyId, UINT nKey1, UINT nKey2);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg LRESULT OnShowNotify(WPARAM _w, LPARAM _l);
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
