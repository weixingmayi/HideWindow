#pragma once

class CTransparentPicCtrl :public CStatic
{
	DECLARE_DYNAMIC(CTransparentPicCtrl)
public:
	CTransparentPicCtrl();
	~CTransparentPicCtrl();

private:
	UINT	m_nResBitmap;
	CSize	m_bmpSize;
	CBitmap m_bitmap;
	CDC		m_memDC;
	int		m_nAlpha;

public:
	void	SetBitmap(UINT _resID);
	void	SetAlpha(int _alpha);//0-100

	DECLARE_MESSAGE_MAP()
	afx_msg void OnPaint();
};