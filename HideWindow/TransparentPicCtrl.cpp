// TransparentPicCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "HideWindow.h"
#include "TransparentPicCtrl.h"


// CTransparentPicCtrl

IMPLEMENT_DYNAMIC(CTransparentPicCtrl, CStatic)

CTransparentPicCtrl::CTransparentPicCtrl():
	m_nResBitmap(0)
	, m_nAlpha(100)
{

}

CTransparentPicCtrl::~CTransparentPicCtrl()
{
}


BEGIN_MESSAGE_MAP(CTransparentPicCtrl, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()



// CTransparentPicCtrl message handlers




void CTransparentPicCtrl::OnPaint()
{
	CPaintDC dc(this);
	CRect rc;
	GetClientRect(rc);

	CDC memDC;
	memDC.CreateCompatibleDC(&dc);
	CBitmap bmp;
	bmp.CreateCompatibleBitmap(&dc, rc.Width(), rc.Height());
	CBitmap* pcurOld = memDC.SelectObject(&bmp);

	memDC.FillSolidRect(rc, RGB(255, 255, 255));
	if (m_nResBitmap)
	{
		if (!m_bitmap.GetSafeHandle())
		{
			m_bitmap.LoadBitmap(m_nResBitmap);
			BITMAP bmp;
			m_bitmap.GetBitmap(&bmp);
			m_bmpSize.cx = bmp.bmWidth;
			m_bmpSize.cy = bmp.bmHeight;
		}
		if (!m_memDC.GetSafeHdc())
		{
			m_memDC.CreateCompatibleDC(&dc);
		}
		CBitmap* pOldBmp = m_memDC.SelectObject(&m_bitmap);
		BLENDFUNCTION blendFUnc;
		blendFUnc.BlendOp = AC_SRC_OVER;
		blendFUnc.BlendFlags = 0;
		blendFUnc.SourceConstantAlpha = m_nAlpha;
		blendFUnc.AlphaFormat = 0;

		memDC.AlphaBlend(0,
			0, 
			rc.Width(), 
			rc.Height(),
			&m_memDC, 
			0, 
			0,
			m_bmpSize.cx,
			m_bmpSize.cy,
			blendFUnc);
		m_memDC.SelectObject(pOldBmp);
	}

	CPen pen(PS_SOLID, 1, RGB(0, 0, 0));
	HGDIOBJ hbr = SelectObject(memDC.GetSafeHdc(), (HBRUSH)GetStockObject(NULL_BRUSH));
	CPen* pOldPen = memDC.SelectObject(&pen);
	memDC.Rectangle(rc);
	SelectObject(memDC.GetSafeHdc(), hbr);
	memDC.SelectObject(pOldPen);

	dc.BitBlt(0, 0, rc.Width(), rc.Height(),
		&memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(pcurOld);
}



void CTransparentPicCtrl::SetAlpha(int _alpha)
{
	m_nAlpha = _alpha;
	if (GetSafeHwnd())
	{
		Invalidate();
	}
}

void CTransparentPicCtrl::SetBitmap(UINT _resID)
{
	ASSERT(!m_nResBitmap);
	m_nResBitmap = _resID;
	if (GetSafeHwnd())
	{
		Invalidate();
	}
}
